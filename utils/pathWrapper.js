
function wrap(path:string):string {
  const prefix = path.startsWith('content://') ? 'RNFetchBlob-content://' : 'RNFetchBlob-file://';
  return prefix + path;
}

export default wrap;