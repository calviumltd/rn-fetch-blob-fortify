//
// Created by Pablo Carrillo on 2019-07-08.
// Copyright (c) 2019 wkh237.github.io. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RNFetchBlobRequestDelegate <NSObject>

- (void) URLSession:(NSURLSession *_Nonnull)session
didReceiveChallenge:(NSURLAuthenticationChallenge *_Nonnull)challenge
  completionHandler:(void (^ _Nonnull) (NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *_Nullable credential))completionHandler;
@end

@protocol RNFetchBlobRequestDelegateOwner <NSObject>

- (id<RNFetchBlobRequestDelegate>_Nullable)RNFetchBlobDelegate;
@end
